package info.hccis.cis2250.canescamper.bo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.cis2250.canescamper.MainActivity;
import info.hccis.cis2250.canescamper.api.JsonCamperApi;
import info.hccis.cis2250.canescamper.ui.campers.CampersFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class CamperContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Camper> CAMPERS = new ArrayList<Camper>();


    /**
     * Load the campers.  This method will use the rest service to provide the data.  The reason it is
     * in this class is because it is changing the value to the CAMPERS list.  This is the list which
     * is used to back the RecyclerView.
     *
     * @author BJM taken from Alex/Thomas' presentation.
     * @since 20200116
     */

    public static void loadCampers(Activity context) {

        Log.d("BJM", "Accessing api at:" + Util.CAMPER_BASE_API);

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.CAMPER_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonCamperApi jsonCamperApi = retrofit.create(JsonCamperApi.class);

        //Create a list of campers.
        Call<List<Camper>> call = jsonCamperApi.getCampers();

        //final reference to activity to get shared preferences
        final Activity contextIn = context;

        call.enqueue(new Callback<List<Camper>>() {

            @Override
            public void onResponse(Call<List<Camper>> call, Response<List<Camper>> response) {

                List<Camper> campers;

                campers = response.body();

                Log.d("bjm", "data back from service call #returned=" + campers.size());

                //**********************************************************************************
                // Now that we have the campers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                CamperContent.CAMPERS.clear();
                CamperContent.CAMPERS.addAll(campers);

                //**********************************************************************************
                // The CamperListFragment has a recyclerview which is used to show the camper list.
                // This recyclerview is backed by the camper list in the CamperContent class.  After
                // this list is loaded, need to notify the adapter from the recyclerview that the
                // data is changed.
                //**********************************************************************************

                CampersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();

                //Remove the progress bar from the view.
                CampersFragment.clearProgressBarVisitiblity();

            }

            @Override
            public void onFailure(Call<List<Camper>> call, Throwable t) {

                //**********************************************************************************
                // If the api call failed, give a notification to the user.
                //**********************************************************************************
                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());

            }
        });
    }
}