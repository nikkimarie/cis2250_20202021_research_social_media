package info.hccis.cis2250.canescamper.ui.feedback;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.net.URLEncoder;
import info.hccis.cis2250.canescamper.R;

public class FeedbackFragment extends Fragment {

    private FeedbackViewModel feedbackViewModel;
    private TextView review;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        feedbackViewModel =
                new ViewModelProvider(this).get(FeedbackViewModel.class);
        View root = inflater.inflate(R.layout.fragment_feedback, container, false);
        //final TextView textView = root.findViewById(R.id.text_feedback);
        feedbackViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        /*
        * Set on click listener when click on submit in feedback fragment to show rating value
        * Date: 2021/01/23
        * Purpose: used for material design presentation - get value of rating bar and display an snackbar with an action button
        * Edited to send user to twitter
        */
        RatingBar ratingBar = root.findViewById(R.id.rating_bar);
        Button btnRate = root.findViewById(R.id.btn_rate);
        TextView review = root.findViewById(R.id.review);

        btnRate.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                    String tweet = review.getText().toString();
                    //creates URL for twitter tweet and sets it to a variable
                    String twitter = String.format("https://twitter.com/compose/tweet?text=%s&url=%s",
                            //creates the tweet text
                            urlEncode(tweet),
                            urlEncode(""));

                    //Send the user to twitter in web browser with the tweet pre typed
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitter));


                    //this starts the intent to open twitter
                    startActivity(intent);
                }


        });
        return root;
    }
    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }
}